package com.itau.filmes;

public class App {
	public static void main(String[] args) {
		Filme[] filmes = Locadora.obterFilmes();
		Impressora.imprimirFilmes(filmes);
	}
}

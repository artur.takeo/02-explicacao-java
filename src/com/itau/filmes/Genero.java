package com.itau.filmes;

public enum Genero {
	COMEDIA("Comédia"),
	ACAO("Ação"),
	DRAMA("Drama"),
	TERROR("Terror"),
	FICCAO("Ficção");
	
	private String nome;
	
	private Genero(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return nome;
	}
}
